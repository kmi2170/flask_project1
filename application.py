import os

from flask import Flask, session, render_template, request
#from flask_session import Session
from flask_socketio import SocketIO, emit
from datetime import datetime

app = Flask(__name__)
#app.config["SECRET_KEY"] = os.getenv("SECRET_KEY")
socketio = SocketIO(app)

#app.config["SESS4ddION_PERMANENT"] = False
#app.config["SESSION_TYPE"] = "filesystem"
#Session(app)

#app.secret_key = "akjeojfko2338hahfewio"
app.secret_key = os.urandom(16)

channel_list = []
channel_data = []
message_body = []
dict = {}
users = []


@app.route("/")
def index():
    return render_template("index.html")
    # if session.get("displayname") is None:
        # return render_template("index.html")
    # else:
        # displayname = session.get("displayname")
        # print(f"/index, displayname: {session['displayname']}")


@app.route("/back")
def back():
    session.pop("displayname", None)
    return render_template("index.html")


@app.route("/display", methods=["POST"])
def display():
    if request.method == "POST":
        displayname = request.form.get("displayname")
        if len(displayname) == 0:
            message = "Display name is empty."
            return render_template("index.html", message=message)

        session["displayname"] = displayname
        if displayname not in users:
            users.append(displayname)
        print(f"/display, displayname: {session['displayname']}")
        print(f"/display, users: {users}")
        return render_template("display.html", displayname=displayname, channels=channel_data)


@app.route("/create", methods=["POST"])
def create():
    message = ""
    channel = request.form.get("channel")
    displayname = session["displayname"]
    print(f"/create, displayname: {session['displayname']}")
    if len(channel) == 0:
        message = "Channel is empty."
        return render_template("display.html", displayname=displayname, channels=channel_data, message=message)

    if channel in channel_list:
        message = "The channel, " + channel + ", already exists."
        return render_template("display.html", displayname=displayname, channels=channel_data, message=message)
    else:
        channel_list.append(channel)
        print(f"/create, channel_list: {channel_list}")
        val = channel_list.index(channel)
        dict[channel] = val
        message_body.append([])

        (time, utime) = get_time()
        channel_data.append({"channel": channel, "time": time, "utime": utime})

        return render_template("display.html", displayname=displayname, channels=channel_data)


@app.route("/channels/<channel>")
def channels(channel):
    displayname = session["displayname"]
    print("route:/channels/channel, displayname: {displayname}")
    val = int(dict[channel])
    messages = message_body[val]
    print("route:/channels/channel")
    print(messages)

    return render_template("channel.html", channel=channel, messages=messages, displayname=displayname)


@socketio.on("send message")
def post(data):
    displayname = session["displayname"]
    message = data["message"]
    channel = data["channel"]
    val = int(dict[channel])

    (time, utime) = get_time()

    messages = {"displayname": displayname, "message": message, "time": time, "utime": utime}
    message_body[val].append(messages)
    if len(message_body[val]) > 100:
        message_body[val].pop(0)

    print(messages)
    print(message_body[val])
    emit("announce message", messages, broadcast=True)


def get_time():
    time = datetime.now()
    utime = datetime.utcnow()
    time = time.strftime('%Y/%m/%d %H:%M:%S')
    utime = utime.strftime('%Y/%m/%d %H:%M:%S')
    return (time, utime)
