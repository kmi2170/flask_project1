document.addEventListener('DOMContentLoaded', () => {

    var socket = io.connect(location.protocol + '//' + document.domain + ':' + location.port);

    document.querySelector('#submit').disabled = true;

    document.querySelector('#message').onkeyup = () => {
        if (document.querySelector('#message').value.length > 0)
            document.querySelector('#submit').disabled = false;
        else
            document.querySelector('#submit').disabled = true;
    };

    const name = document.querySelector('#displayname').innerHTML;
    document.getElementsByName(name).forEach((item) => {
            item.hidden = false;
    });


    socket.on('connect', () => {
        document.querySelector('#submit').onclick = () => {
            //console.log('click submit');
            const message = document.querySelector('#message').value;
            const channel = document.querySelector('#channel').innerHTML;
            //console.log(message);
            //console.log(channel);
            //const date = new Date();
            //ldate = date.toLocaleString();
            //socket.emit('send message', {'message': message, 'date': date});
            socket.emit('send message', {'message': message, 'channel': channel});
        };
    });

    socket.on('announce message', data => {
        var d = document.createElement('div');
        const p1 = document.createElement('p');
        const p2 = document.createElement('pre');
        p1.innerHTML = `&gt;&gt; <b style="color:blue">${data.displayname}</b>: <em style="color:grey">posted at ${data.time}</em> \
        <button type="button" name="${data.displayname }" onclick="this.parentNode.parentNode.parentNode.removeChild(this.parentNode.parentNode);" hidden>Remove</button>`;
        p2.innerHTML = `${data.message}`;
        d.append(p1)
        d.append(p2)
        //document.querySelector('#contents').append(p1);
        //document.querySelector('#contents').append(p2);
        document.querySelector('#contents').append(d);

        document.querySelector('#message').value = '';
        document.querySelector('#submit').disabled = true;

        const name = document.querySelector('#displayname').innerHTML;
        document.getElementsByName(name).forEach((item) => {
            item.hidden = false;
        });
    });
});
